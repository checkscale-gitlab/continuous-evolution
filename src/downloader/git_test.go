package downloader

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"errors"
	"testing"
)

func TestGitAccept(t *testing.T) {
	confGit := GitConfig{Enabled: true, GitlabURL: []string{"gitlab.com", "domain.com"}, GithubURL: []string{"github.com", "other.com"}}
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: confGit, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	g := newGit(conf).(git)
	toTest := []struct {
		name       string
		url        string
		shouldPass bool
	}{
		{name: "empty", url: "", shouldPass: false},
		{name: "without basic auth", url: "https://gitlab.com/ContinuousEvolution/continuous-evolution.git", shouldPass: false},
		{name: "git url", url: "git@gitlab.com:ContinuousEvolution/continuous-evolution.git", shouldPass: false},
		{name: "good gitlab url", url: "https://login:token@gitlab.com/ContinuousEvolution/continuous-evolution.git", shouldPass: true},
		{name: "good other url", url: "https://login:token@domain.com/ContinuousEvolution/continuous-evolution.git", shouldPass: true},
		{name: "good github url", url: "https://login:token@github.com/ContinuousEvolution/continuous-evolution.git", shouldPass: true},
		{name: "good other github url", url: "https://login:token@other.com/ContinuousEvolution/continuous-evolution.git", shouldPass: true},
		{name: "unknown domain", url: "https://login:token@unknown.com/ContinuousEvolution/continuous-evolution.git", shouldPass: false},
	}
	for _, test := range toTest {
		t.Run(test.name, func(t *testing.T) {
			accept := g.accept(test.url)
			if accept && !test.shouldPass {
				t.Fatal("should not accept bad url")
			} else if !accept && test.shouldPass {
				t.Fatal("should accept good url")
			}
		})
	}
}

func TestBuildProject(t *testing.T) {
	confGit := GitConfig{Enabled: true, GitlabURL: []string{"my.gitlab.com"}}
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: confGit, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	g := newGit(conf).(git)
	gitURL := "https://login:token@my.gitlab.com/ContinuousEvolution/continuous-evolution.git"
	p, err := g.buildProject(gitURL)
	if err != nil {
		t.Fatal("git should not throw error when good https git url", err)
	}
	if p.Login() != "login" {
		t.Fatalf("git should return good login instead of %s", p.Login())
	}
	if p.Token() != "token" {
		t.Fatalf("git should return good token instead of %s", p.Token())
	}
	if p.Host() != "my.gitlab.com" {
		t.Fatalf("git should return good host instead of %s", p.Host())
	}
	if p.TypeHost() != project.Gitlab {
		t.Fatalf("git should return good typehost instead of %s", p.TypeHost())
	}
	if p.Organisation() != "ContinuousEvolution" {
		t.Fatalf("git should return good organisation instead of %s", p.Organisation())
	}
	if p.Name() != "continuous-evolution" {
		t.Fatalf("git should return good name instead of %s", p.Name())
	}
	if p.GitURL() != gitURL {
		t.Fatalf("git should return good url instead of %s", p.GitURL())
	}
	if p.AbsoluteDirectoy() != "/pathtowrite/continuous-evolution" {
		t.Fatalf("git should take path to write from config instead of %s", p.AbsoluteDirectoy())
	}
	if p.BranchName() != "branchName" {
		t.Fatalf("git should take branch name from config instead of %s", p.BranchName())
	}
}

func TestGitDownload(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	g := newGit(conf).(git)
	fakeGit := &mocks.FakeGit{
		CloneHook: func() error {
			return nil
		},
	}

	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.GitHook = func() (ident1 project.Git) {
		return fakeGit
	}

	p, err := g.download(fakeProject)
	if p == nil {
		t.Fatal("Good project should be return")
	}
	if err != nil {
		t.Fatal("Good project should not throw error")
	}
	fakeProject.AssertGitCalledOnce(t)
	fakeGit.AssertCloneCalledOnce(t)
}

func TestGitDownloadError(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	g := newGit(conf).(git)
	fakeGit := &mocks.FakeGit{
		CloneHook: func() error {
			return errors.New("fake git clone error")
		},
	}
	fakeProject := &mocks.FakeProject{
		GitHook: func() (ident1 project.Git) {
			return fakeGit
		},
	}
	p, err := g.download(fakeProject)
	if p == nil {
		t.Fatal("Error when git clone should not return nil project")
	}
	if err == nil {
		t.Fatal("Git downloader should return error from git")
	}
	fakeProject.AssertGitCalledOnce(t)
	fakeGit.AssertCloneCalledOnce(t)
}
