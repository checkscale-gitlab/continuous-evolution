package io

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

var loggerHTTP = logrus.WithField("logger", "project/io/http")

//HTTP is the struct to make http call with project
type HTTP struct {
	login         string
	token         string
	requestFabric func(method, url string, body io.Reader) (*http.Request, error)
	do            func(req *http.Request) (*http.Response, error)
}

//NewHTTP return an instance to make http call with project
func NewHTTP(login string, token string) *HTTP {
	client := &http.Client{}
	return &HTTP{
		login:         login,
		token:         token,
		requestFabric: http.NewRequest,
		do:            client.Do,
	}
}

func (h *HTTP) send(req *http.Request, url string, unmarshalJson interface{}) error {
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(h.login, h.token)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()
	resp, err := h.do(req.WithContext(ctx))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	loggerHTTP.WithFields(logrus.Fields{
		"status":  resp.Status,
		"headers": resp.Header,
	}).Info("Response from host")

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("Bad status code %d", resp.StatusCode)
	}

	bodyRet, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		loggerHTTP.WithField("body", string(bodyRet)).WithError(err).Error("Body from host")
		return err
	}

	if unmarshalJson != nil {
		err = json.Unmarshal(bodyRet, unmarshalJson)
		if err != nil {
			loggerHTTP.WithError(err).Error("Can't unmarshal json body")
			return err
		}
	}

	return nil
}

func (h *HTTP) sendBody(marshalJson interface{}) (io.Reader, error) {
	if marshalJson == nil {
		return nil, nil
	}
	body, err := json.Marshal(marshalJson)
	if err != nil {
		return nil, err
	}
	return bytes.NewBuffer(body), nil
}

//Get make an http.Get with BasicAuth of project.Login, project.Token
func (h *HTTP) Get(url string, unmarshalJson interface{}) error {
	req, err := h.requestFabric("GET", url, nil)
	if err != nil {
		return err
	}
	return h.send(req, url, unmarshalJson)
}

//Post make an http.Post with BasicAuth of project.Login, project.Token
func (h *HTTP) Post(url string, marshalJson interface{}, unmarshalJson interface{}) error {
	body, err := h.sendBody(marshalJson)
	if err != nil {
		return err
	}
	req, err := h.requestFabric("POST", url, body)
	if err != nil {
		return err
	}
	err = h.send(req, url, unmarshalJson)
	if err != nil {
		b, _ := h.sendBody(marshalJson)
		data, _ := ioutil.ReadAll(b)
		loggerHTTP.WithField("bodySend", string(data)).Error("bad request")
	}
	return err
}

//Patch make an http.Patch with BasicAuth of project.Login, project.Token
func (h *HTTP) Patch(url string, marshalJson interface{}, unmarshalJson interface{}) error {
	body, err := h.sendBody(marshalJson)
	if err != nil {
		return err
	}
	req, err := h.requestFabric("PATCH", url, body)
	if err != nil {
		return err
	}
	err = h.send(req, url, unmarshalJson)
	if err != nil {
		loggerHTTP.WithField("bodySend", marshalJson).Error("bad request")
	}
	return err
}

//Put make an http.Put with BasicAuth of project.Login, project.Token
func (h *HTTP) Put(url string, marshalJson interface{}, unmarshalJson interface{}) error {
	body, err := h.sendBody(marshalJson)
	if err != nil {
		return err
	}
	req, err := h.requestFabric("PUT", url, body)
	if err != nil {
		return err
	}
	err = h.send(req, url, unmarshalJson)
	if err != nil {
		loggerHTTP.WithField("bodySend", marshalJson).Error("bad request")
	}
	return err
}
