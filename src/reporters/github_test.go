package reporters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"encoding/json"
	"html/template"
	"testing"
)

type dataTest struct {
	name     string
	host     string
	typehost project.TypeHost
	login    string
	token    string
	gitDiff  bool
	accept   bool
}

func TestGithubAccept(t *testing.T) {
	gh := newGithub(DefaultConfig)

	data := []dataTest{
		{name: "Normal", host: "github.com", typehost: project.Github, login: "login", token: "token", gitDiff: true, accept: true},
		{name: "Not github", host: "github.com", typehost: project.Gitlab, login: "login", token: "token", gitDiff: true, accept: false},
		{name: "Empty host", host: "", typehost: project.Github, login: "login", token: "token", gitDiff: true, accept: false},
		{name: "Empty login", host: "github.com", typehost: project.Github, login: "", token: "token", gitDiff: true, accept: false},
		{name: "Empty token", host: "github.com", typehost: project.Github, login: "login", token: "", gitDiff: true, accept: false},
		{name: "No git diff", host: "github.com", typehost: project.Github, login: "login", token: "token", gitDiff: false, accept: false},
	}

	for _, d := range data {
		t.Run(d.name, func(t *testing.T) {
			fakeGit := &mocks.FakeGit{
				DiffHook: func() bool {
					return d.gitDiff
				},
			}
			fakeProject := &mocks.FakeProject{
				HostHook: func() string {
					return d.host
				},
				TypeHostHook: func() project.TypeHost {
					return d.typehost
				},
				LoginHook: func() string {
					return d.login
				},
				TokenHook: func() string {
					return d.token
				},
				GitHook: func() (ident1 project.Git) {
					return fakeGit
				},
			}
			if d.accept && !gh.Accept(fakeProject) {
				t.Fatal("github should accept good project")
			} else if !d.accept && gh.Accept(fakeProject) {
				t.Fatal("github should not accept bad project")
			}
		})
	}
}

func TestGithubReport(t *testing.T) {
	gh := newGithub(DefaultConfig)

	fakeGit := mocks.NewFakeGitDefaultFatal(t)
	fakeGit.DefaultBranchHook = func() (string, error) {
		return "master", nil
	}
	fakeGit.CommitHook = func() error {
		return nil
	}
	fakeGit.PushForceHook = func() error {
		return nil
	}

	fakeHTTP := mocks.NewFakeHTTPDefaultFatal(t)
	fakeHTTP.PostHook = func(url string, toSend interface{}, toReceive interface{}) error {
		if url != "https://api.typehost.com/repos/organisation/name/pulls" {
			t.Fatalf("Url to post new pull-request is not good %s", url)
		}
		json.Unmarshal([]byte(`{"number": 42}`), toReceive)
		return nil
	}
	fakeHTTP.PatchHook = func(url string, toSend interface{}, toReceive interface{}) error {
		if url != "https://api.typehost.com/repos/organisation/name/pulls/42" {
			t.Fatalf("Url to patch body of pull-request is not good %s", url)
		}
		return nil
	}

	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.GitHook = func() project.Git {
		return fakeGit
	}
	fakeProject.HTTPHook = func() project.HTTP {
		return fakeHTTP
	}
	reprocessDistantID := ""
	fakeProject.ReProcessDistantIDHook = func() string {
		return reprocessDistantID
	}
	fakeProject.SetReProcessDistantIDHook = func(id string) project.Project {
		reprocessDistantID = id
		return fakeProject
	}
	fakeProject.NameHook = func() string {
		return "name"
	}
	fakeProject.OrganisationHook = func() string {
		return "organisation"
	}
	fakeProject.HostHook = func() string {
		return "typehost.com"
	}
	fakeProject.SetURLReProcessHook = func(url string) project.Project {
		return fakeProject
	}
	fakeProject.PackagesHook = func() []project.Package {
		return make([]project.Package, 0)
	}
	fakeProject.URLReProcessHook = func() string {
		return "urlReprocess"
	}
	fakeProject.FullnameHook = func() string {
		return "fullname"
	}
	fakeProject.BranchNameHook = func() string {
		return "master"
	}
	fakeProject.LoginHook = func() string {
		return "login"
	}
	fakeProject.TokenHook = func() string {
		return "token"
	}
	fakeProject.SetGitURLHook = func(url string) project.Project {
		return fakeProject
	}

	tplMergeRequestBody, err := template.New("MergeRequest").Parse(project.MergeRequestBody)
	if err != nil {
		t.Fatal("Can't parse project template")
	}
	_, err = gh.Report(fakeProject, tplMergeRequestBody)
	if err != nil {
		t.Fatal("Valid report should not throw error", err)
	}
}
