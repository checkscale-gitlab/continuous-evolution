package main

import (
	"continuous-evolution/src/deleter"
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/orchestrator"
	"continuous-evolution/src/reporters"
	"continuous-evolution/src/scheduler"
	"continuous-evolution/src/updaters"
	"flag"
	"os"
	"runtime"

	"github.com/sirupsen/logrus"
)

var (
	logger  = logrus.WithField("logger", "main")
	version string // set at build time
	commit  string // set at build time
	date    string // set at build time
)

func main() {
	var showVersion bool
	flag.BoolVar(&showVersion, "version", false, "Show current version and build time")
	var generateDefaultConfig string
	flag.StringVar(&generateDefaultConfig, "generate-config", "", "The path where you want to generate default configuration (example : ./configuration.toml)")
	var url string
	flag.StringVar(&url, "url", "", "The url of the project to update")
	var configPath string
	flag.StringVar(&configPath, "config", "", "Start with config file (required for web mode)")
	var noReport bool
	flag.BoolVar(&noReport, "no-report", false, "Don't report modification on code")
	flag.Parse()

	if showVersion {
		logrus.WithFields(logrus.Fields{
			"version":    version,
			"commit":     commit,
			"build time": date,
			"go version": runtime.Version(),
		}).Warn("Welcome to Continuous-Evolution")
		return
	}

	if generateDefaultConfig != "" {
		if err := WriteConf(generateDefaultConfig); err != nil {
			if err != nil {
				logger.WithError(err).Error("Can't generate default configuration")
				os.Exit(1)
				return
			}
		}
		return
	}

	config, err := GetConfig(configPath)
	if err != nil {
		logger.WithError(err).Error("Bad configuration file")
		os.Exit(1)
		return
	}

	schedulerManager, err := scheduler.NewScheduler(config.Scheduler)
	if err != nil {
		logger.WithError(err).Error("Scheduler can't start")
		os.Exit(1)
		return
	}

	downloaderManager := downloader.NewManager(config.Downloader, schedulerManager, schedulerManager)
	updaterManager := updaters.NewManager(config.Updater)
	reporterManager, err := reporters.NewManager(config.Reporter)
	if err != nil {
		logger.WithError(err).Error("Reporter can't start")
		os.Exit(1)
		return
	}

	orchestratorConf := config.Orchestrator
	if url != "" {
		orchestratorConf = orchestrator.DefaultConfig
		orchestratorConf.Web.Enabled = false
		orchestratorConf.Cli.Enabled = true
		orchestratorConf.Cli.Path = url
	}

	workers := []orchestrator.WorkerConf{
		{Active: config.Scheduler.Enabled, PoolSize: 1, Callback: schedulerManager.Save(scheduler.CreateState)},
		{Active: true, PoolSize: config.Downloader.PoolSize, Callback: downloaderManager.Download},
		{Active: config.Scheduler.Enabled, PoolSize: 1, Callback: schedulerManager.Save(scheduler.DownloadState)},
		{Active: true, PoolSize: config.Updater.PoolSize, Callback: updaterManager.PackageFromFile},
		{Active: config.Scheduler.Enabled, PoolSize: 1, Callback: schedulerManager.Save(scheduler.PackageFromFileState)},
		{Active: true, PoolSize: config.Updater.PoolSize, Callback: updaterManager.Update},
		{Active: config.Scheduler.Enabled, PoolSize: 1, Callback: schedulerManager.Save(scheduler.UpdateState)},
		{Active: !noReport && config.Reporter.Enabled, PoolSize: config.Reporter.PoolSize, Callback: reporterManager.Report},
		{Active: config.Scheduler.Enabled, PoolSize: 1, Callback: schedulerManager.Save(scheduler.ReportState)},
		{Active: orchestratorConf.Web.Enabled, PoolSize: 1, Callback: deleter.Delete}, //activate only in web mode
		{Active: orchestratorConf.Web.Enabled && config.Scheduler.Enabled, PoolSize: 1, Callback: schedulerManager.Save(scheduler.DeleteState)},
	}

	orchestratorManager, err := orchestrator.NewManager(orchestratorConf, workers)
	if err != nil {
		logger.WithError(err).Error("Orchestrator can't start")
		os.Exit(1)
		return
	}

	if config.Scheduler.Enabled {
		schedulerManager.Start(orchestratorManager, downloaderManager)
		defer schedulerManager.Close()
	}

	err = orchestratorManager.Run(downloaderManager, schedulerManager)
	if err != nil {
		logger.WithError(err).Error("Orchestrator can't run")
		os.Exit(1)
	}

}
