package orchestrator

import (
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/project"
	"continuous-evolution/src/project/io"
	"errors"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

//GitlabConfig is configuration required by gitlab to add project based on global user
type GitlabConfig struct {
	Enabled       bool
	DurationFetch string
	Instances     []GitlabInstance
}

//GitlabInstance represent a call to gitlab (e.g. url + token)
type GitlabInstance struct {
	APIURL       string
	Login        string
	PrivateToken string
}

//DefaultGitlabConfig is the default configuration required by gitlab
var DefaultGitlabConfig = GitlabConfig{Enabled: false, DurationFetch: "1h", Instances: []GitlabInstance{{APIURL: "https://gitlab.com/api/v4", Login: "", PrivateToken: ""}}}

var loggerGitlab = logrus.WithField("logger", "orchestrator/gitlab")

type gitlab struct {
	config        GitlabConfig
	durationFetch time.Duration
	sender        Sender
	poison        chan bool
	httpFactory   func(login string, token string) project.HTTP
}

func newGitlab(config Config, sender Sender) (*gitlab, error) {
	d, err := time.ParseDuration(config.Gitlab.DurationFetch)
	if err != nil {
		return nil, err
	}
	for _, instance := range config.Gitlab.Instances {
		if instance.Login == "" || instance.PrivateToken == "" {
			return nil, errors.New("the instance " + instance.APIURL + " must have login and token")
		}
	}
	return &gitlab{
		config:        config.Gitlab,
		durationFetch: d,
		sender:        sender,
		poison:        make(chan bool),
		httpFactory: func(login string, token string) project.HTTP {
			return io.NewHTTP(login, token)
		},
	}, nil
}

//start launch in goroutine every $config.DurationFetch a fetch to get data
func (g *gitlab) start(downloaderManager downloader.Manager, projectAlreadySave ProjectAlreadySave) error {
	if !projectAlreadySave.IsEnabled() {
		return errors.New("you can't enable orchestrator.gitlab without enable scheduler")
	}
	go func() {
		if err := g.fetch(downloaderManager, projectAlreadySave); err != nil {
			loggerGitlab.WithError(err).Error("bad fetch")
		}
		for {
			select {
			case <-time.Tick(g.durationFetch):
				if err := g.fetch(downloaderManager, projectAlreadySave); err != nil {
					loggerGitlab.WithError(err).Error("bad fetch")
				}
			case <-g.poison:
				return
			}
		}
	}()
	loggerWeb.WithField("duration", g.config.DurationFetch).Info("Server get projects from gitlab")
	return nil
}

type gitlabPermission struct {
	AccessLevel int `json:"access_level"`
}

type gitlabPermissions struct {
	ProjectAccess *gitlabPermission `json:"project_access"`
	GroupAccess   *gitlabPermission `json:"group_access"`
}

type gitlabAPIProject struct {
	HTTPURLToRepo string            `json:"http_url_to_repo"`
	Permissions   gitlabPermissions `json:"permissions"`
}

func (g *gitlab) fetch(downloaderManager downloader.Manager, projectAlreadySave ProjectAlreadySave) error {
	for _, instance := range g.config.Instances {
		httpClient := g.httpFactory(instance.Login, instance.PrivateToken)
		projectsURL := make([]gitlabAPIProject, 0)
		err := httpClient.Get(instance.APIURL+"/projects?membership=true&with_merge_requests_enabled=true&private_token="+instance.PrivateToken, &projectsURL)
		if err != nil {
			loggerGitlab.WithError(err).Error("Can't fetch projects")
			continue
		}
		loggerGitlab.WithField("nb", len(projectsURL)).Info("New projects on gitlab")
		for _, projectURL := range projectsURL {
			if (projectURL.Permissions.GroupAccess != nil && projectURL.Permissions.GroupAccess.AccessLevel >= 30) ||
				(projectURL.Permissions.ProjectAccess != nil && projectURL.Permissions.ProjectAccess.AccessLevel >= 30) {
				urls := strings.Split(projectURL.HTTPURLToRepo, "://")
				pro, err := downloaderManager.BuildProject(urls[0] + "://" + instance.Login + ":" + instance.PrivateToken + "@" + urls[1])
				if err != nil {
					loggerGitlab.WithField("project", projectURL.HTTPURLToRepo).WithError(err).Error("skip project because can't be created")
				} else {
					if !projectAlreadySave.Exist(pro) {
						loggerGitlab.WithField("project", pro.Fullname()).Info("New project on gitlab")
						g.sender.Send(project.NewOptional(pro))
					}
				}
			} else {
				loggerGitlab.WithField("project", projectURL.HTTPURLToRepo).Warn("unsiffecent right to create MR")
			}
		}
	}
	return nil
}

func (g *gitlab) close() {
	g.poison <- true
	close(g.poison)
	g.sender.Close()
}
