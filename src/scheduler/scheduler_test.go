package scheduler

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"encoding/json"
	"io/ioutil"
	"testing"
	"time"
)

func initSchedulerFile(t *testing.T, data []minimalProject) string {
	filePath, err := ioutil.TempDir("", "ConEvolTest")
	if err != nil {
		t.Fatal("Can't create temporary file", err)
	}

	json, err := json.Marshal(data)
	if err != nil {
		t.Fatal("can't marshal data", err)
	}
	err = ioutil.WriteFile(filePath+"/scehduler", json, 0644)
	if err != nil {
		t.Fatal("Can't write file", err)
	}

	return filePath + "/scehduler"
}

func buildMetrics(t time.Time) []metric {
	return []metric{{DeleteDate: t}}
}

func TestBadConfig(t *testing.T) {
	config := Config{Enabled: true, WaitDurationProcess: "1s"}
	_, err := NewScheduler(config)
	if err == nil {
		t.Fatal("Bad config (no PathDb) must return an error")
	}
	config = Config{Enabled: true, PathDb: initSchedulerFile(t, []minimalProject{}), WaitDurationProcess: "a"}
	_, err = NewScheduler(config)
	if err == nil {
		t.Fatal("Bad config (ba wait time) must return an error")
	}
}

func TestNextProject(t *testing.T) {
	config := Config{Enabled: true, PathDb: initSchedulerFile(t, []minimalProject{}), WaitDurationProcess: "100s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid", err)
	}
	s.(*scheduler).projects = []minimalProject{{Name: "test", GitURL: "test", Metrics: buildMetrics(time.Now().Add(-10 * time.Second))}}
	ok, p, duration := s.(*scheduler).nextProjectToUpdate()
	if !ok {
		t.Fatal("Scheduler with project must found a next project")
	}
	if p.Name != "test" {
		t.Fatal("Scheduler must select valid project test instead of", p.Name)
	}
	if int(duration.Seconds()) < 89 && int(duration.Seconds()) < 91 {
		t.Fatal("Scheduler must sleep during 90sc before update project instead of", duration.Seconds())
	}
}

func TestNextProjectWith2Projects(t *testing.T) {
	config := Config{Enabled: true, PathDb: initSchedulerFile(t, []minimalProject{}), WaitDurationProcess: "100s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid")
	}
	s.(*scheduler).projects = []minimalProject{
		{Name: "test", GitURL: "test", Host: "domain.com", Organisation: "organisation", Metrics: buildMetrics(time.Now().Add(-10 * time.Second))},
		{Name: "test2", GitURL: "test2", Host: "domain.com", Organisation: "organisation", Metrics: buildMetrics(time.Now().Add(-5 * time.Second))},
	}
	ok, p, duration := s.(*scheduler).nextProjectToUpdate()
	if !ok {
		t.Fatal("Scheduler with project must found a next project")
	}
	if p.Name != "test" {
		t.Fatal("Scheduler must select valid project test instead of", p.Name)
	}
	if int(duration.Seconds()) > 89 && int(duration.Seconds()) < 91 {
		t.Fatal("Scheduler must sleep during 90sc before update project instead of", duration.Seconds())
	}
	p2 := project.New(project.Input{
		Name:         p.Name,
		GitURL:       p.GitURL,
		BranchName:   "branchName",
		PathToWrite:  "/tmp",
		Login:        "login",
		Token:        "token",
		Host:         p.Host,
		Organisation: p.Organisation,
	})
	s.Save(CreateState)(project.NewOptional(p2))
	ok, p, duration = s.(*scheduler).nextProjectToUpdate()
	if !ok {
		t.Fatal("Scheduler with project must found a next project")
	}
	if p.Name != "test2" {
		t.Fatal("Scheduler must select valid project test2 instead of", p.Name)
	}
	if int(duration.Seconds()) > 94 && int(duration.Seconds()) < 96 {
		t.Fatal("Scheduler must sleep during 90sc before update project instead of", duration.Seconds())
	}
}

func TestStart(t *testing.T) {
	chanOk := make(chan project.Project, 1)
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(o project.Option) {
		chanOk <- o.Get()
	}
	fakeDownloaderManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloaderManager.BuildProjectHook = func(url string) (project.Project, error) {
		return project.New(project.Input{
			Name:         "test",
			GitURL:       url,
			BranchName:   "branchName",
			PathToWrite:  "/tmp",
			Login:        "login",
			Token:        "token",
			Host:         "domain.com",
			Organisation: "organisation",
		}), nil
	}
	config := Config{Enabled: true, PathDb: initSchedulerFile(t, []minimalProject{}), WaitDurationProcess: "1s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid")
	}
	s.(*scheduler).projects = []minimalProject{{Name: "test", GitURL: "test", Metrics: buildMetrics(time.Now().Add(-1 * time.Second))}}
	s.Start(fakeSender, fakeDownloaderManager)
	select {
	case p := <-chanOk:
		if p.Name() != "test" {
			t.Fatal("Scheduler must select valid project test instead of", p.Name())
		}
	case <-time.After(5 * time.Second):
		t.Fatal("Scheduler must start new project chain before 5 seconds")
	}
}

func TestStop(t *testing.T) {
	chanOk := make(chan project.Project, 1)
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(o project.Option) {
		chanOk <- o.Get()
	}
	fakeDownloaderManager := mocks.NewFakeManagerDefaultFatal(t)
	config := Config{Enabled: true, PathDb: initSchedulerFile(t, []minimalProject{}), WaitDurationProcess: "100s"}
	s, err := NewScheduler(config)
	if err != nil {
		t.Fatal("DefaultConfig must be valid")
	}
	s.(*scheduler).projects = []minimalProject{{Name: "test", GitURL: "test", Metrics: buildMetrics(time.Now().Add(-1 * time.Second))}}
	s.Start(fakeSender, fakeDownloaderManager)
	go func() {
		time.Sleep(10 * time.Millisecond)
		s.Close()
	}()
	select {
	case <-chanOk:
		t.Fatal("Scheduler should not launch chain if we call Close()")
	case <-time.After(100 * time.Millisecond):
	}
}

func TestStore(t *testing.T) {
	chanOk := make(chan project.Project, 1)
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(o project.Option) {
		chanOk <- o.Get()
	}
	fakeDownloaderManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloaderManager.BuildProjectHook = func(url string) (project.Project, error) {
		return project.New(project.Input{
			Name:         "test",
			GitURL:       url,
			BranchName:   "branchName",
			PathToWrite:  "/tmp",
			Login:        "login",
			Token:        "token",
			Host:         "domain.com",
			Organisation: "organisation",
		}), nil
	}
	p := minimalProject{
		Name:         "test",
		GitURL:       "test",
		Host:         "domain.com",
		Organisation: "organisation",
		Metrics:      buildMetrics(time.Now()),
	}

	pathDb := initSchedulerFile(t, []minimalProject{p})

	s, err := NewScheduler(Config{Enabled: true, PathDb: pathDb, WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid", err)
	}
	s.Start(fakeSender, fakeDownloaderManager)
	select {
	case p := <-chanOk:
		if p.Name() != "test" {
			t.Fatal("Scheduler must select valid project test instead of", p.Name())
		}
	case <-time.After(5 * time.Second):
		t.Fatal("Scheduler must start new project chain before 5 seconds")
	}
	p2 := project.New(project.Input{
		Name:         "test2",
		GitURL:       "test2",
		BranchName:   "branchName",
		PathToWrite:  "/tmp",
		Login:        "login",
		Token:        "token",
		Host:         "domain.com",
		Organisation: "organisation",
	})
	s.Save(CreateState)(project.NewOptional(p2))
	s.Close()
	s, err = NewScheduler(Config{Enabled: true, PathDb: pathDb, WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid", err)
	}
	s.Start(fakeSender, fakeDownloaderManager)
	if len(s.(*scheduler).projects) != 2 {
		t.Fatalf("test2 should be saved in file only %d saved instead of 2", len(s.(*scheduler).projects))
	}
	if s.(*scheduler).projects[0].GitURL != "test" {
		t.Fatalf("test should be saved instead of %s", s.(*scheduler).projects[0].GitURL)
	}
	if s.(*scheduler).projects[1].GitURL != "test2" {
		t.Fatal("test2 should be saved")
	}
	s.Delete(p2)
	if len(s.(*scheduler).projects) != 2 {
		t.Fatal("test2 should not be removed in file but updated")
	}
	if s.(*scheduler).projects[1].GitURL != "test2" {
		t.Fatal("test2 should not be removed in file but updated")
	}
	if s.(*scheduler).projects[1].DeletedDate == nil {
		t.Fatal("test2 should marked has deleted")
	}
	s.Close()
	s, err = NewScheduler(Config{Enabled: true, PathDb: pathDb, WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid")
	}
	s.Start(fakeSender, fakeDownloaderManager)
	s.Close()
	if len(s.(*scheduler).projects) != 2 {
		t.Fatal("test2 should be removed in file")
	}
	if s.(*scheduler).projects[0].GitURL != "test" {
		t.Fatal("test should be saved")
	}
	if s.(*scheduler).projects[1].GitURL != "test2" {
		t.Fatal("test2 should not be removed in file but updated")
	}
	if s.(*scheduler).projects[1].DeletedDate == nil {
		t.Fatal("test2 should marked has deleted")
	}
}

func TestDelete(t *testing.T) {
	chanOk := make(chan project.Project, 1)
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(o project.Option) {
		chanOk <- o.Get()
	}
	fakeDownloaderManager := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloaderManager.BuildProjectHook = func(url string) (project.Project, error) {
		return project.New(project.Input{
			Name:         "test",
			GitURL:       url,
			BranchName:   "branchName",
			PathToWrite:  "/tmp",
			Login:        "login",
			Token:        "token",
			Host:         "domain.com",
			Organisation: "organisation",
		}), nil
	}
	p := minimalProject{
		Name:         "test",
		GitURL:       "test",
		Host:         "domain.com",
		Organisation: "organisation",
		DeletedDate:  nil,
		Metrics:      buildMetrics(time.Now()),
	}
	now := time.Now()
	p2 := minimalProject{
		Name:         "test",
		GitURL:       "test",
		Host:         "domain.com",
		Organisation: "organisation",
		DeletedDate:  &now,
		Metrics:      buildMetrics(time.Now().Add(-1 * time.Hour)),
	}

	pathDb := initSchedulerFile(t, []minimalProject{p, p2})

	s, err := NewScheduler(Config{Enabled: true, PathDb: pathDb, WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid")
	}
	s.Start(fakeSender, fakeDownloaderManager)
	select {
	case p := <-chanOk:
		if p.Name() != "test" {
			t.Fatal("Scheduler must select no deleted project instead of", p.Name())
		}
	case <-time.After(5 * time.Second):
		t.Fatal("Scheduler must start new project chain before 5 seconds")
	}
}

func TestExist(t *testing.T) {
	p := minimalProject{
		Name:         "test",
		GitURL:       "test",
		Host:         "domain.com",
		Organisation: "organisation",
		DeletedDate:  nil,
		Metrics:      buildMetrics(time.Now()),
	}

	pathDb := initSchedulerFile(t, []minimalProject{p})

	s, err := NewScheduler(Config{Enabled: true, PathDb: pathDb, WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid")
	}
	p2 := project.New(project.Input{
		Name:         "test",
		GitURL:       "test",
		BranchName:   "branchName",
		PathToWrite:  "/tmp",
		Login:        "login",
		Token:        "token",
		Host:         "domain.com",
		Organisation: "organisation",
	})
	if !s.Exist(p2) {
		t.Fatal("Project should exist")
	}
	p3 := project.New(project.Input{
		Name:         "test2",
		GitURL:       "test2",
		BranchName:   "branchName",
		PathToWrite:  "/tmp",
		Login:        "login",
		Token:        "token",
		Host:         "domain.com",
		Organisation: "organisation",
	})
	if s.Exist(p3) {
		t.Fatal("Project should not exist")
	}
}

func TestRetreive(t *testing.T) {
	p := minimalProject{
		Name:         "test",
		GitURL:       "test",
		Host:         "domain.com",
		Organisation: "organisation",
		Login:        "login",
		Token:        "token",
		DeletedDate:  nil,
		Metrics:      buildMetrics(time.Now()),
	}

	pathDb := initSchedulerFile(t, []minimalProject{p})

	s, err := NewScheduler(Config{Enabled: true, PathDb: pathDb, WaitDurationProcess: "1s"})
	if err != nil {
		t.Fatal("Valid config must be valid")
	}

	if login, token, ok := s.Retrieve("domain.com", "organisation", "test"); !ok {
		t.Fatal("Project should exist")
	} else if login != "login" {
		t.Fatalf("Login should be login instead of %s", login)
	} else if token != "token" {
		t.Fatalf("Token should be token instead of %s", token)
	}

	if login, token, ok := s.Retrieve("domain.com", "organisation", "test2"); ok {
		t.Fatal("Project should not exist")
	} else if login != "" {
		t.Fatalf("Login should be empty instead of %s", login)
	} else if token != "" {
		t.Fatalf("Token should be empty instead of %s", token)
	}
}
