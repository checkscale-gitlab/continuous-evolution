package downloader

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"os"
	"testing"
)

func TestLocalAccept(t *testing.T) {
	l := newLocal(DefaultConfig).(local)
	if l.accept("") {
		t.Fatal("local should not accept empty string")
	}
	if l.accept("/") {
		t.Fatal("local should not accept path without git")
	}
	if l.accept("/continuous/") {
		t.Fatal("local should not accept no existing path")
	}
	err := os.MkdirAll("/tmp/testlocal/", os.ModePerm)
	if err != nil {
		t.Fatal("Can't create /tmp/testlocal/ dir !?")
	}
	if l.accept("/tmp/testlocal") {
		t.Fatal("local should not accept no ending / path")
	}
	if l.accept("../testlocal") {
		t.Fatal("local should not accept relatif path")
	}
	err = os.MkdirAll("/tmp/testlocal/.git", os.ModePerm)
	if err != nil {
		t.Fatal("Can't create /tmp/testlocal/.git dir !?")
	}
	if !l.accept("/tmp/testlocal/") {
		t.Fatal("local should accept good path")
	}
	err = os.RemoveAll("/tmp/testlocal")
	if err != nil {
		t.Fatal("Can't remove /tmp/testlocal !?", err)
	}
}

func TestLocalBuild(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	l := newLocal(conf).(local)
	path := "/tmp/testlocal/"
	err := os.MkdirAll(path+".git", os.ModePerm)
	if err != nil {
		t.Fatalf("Can't create %s.git dir !?", path)
	}
	p, err := l.buildProject(path)
	if err != nil {
		t.Fatal("local should not throw error when good path", err)
	}
	if p.Login() != "" {
		t.Fatalf("local should not return login instead of %s", p.Login())
	}
	if p.Token() != "" {
		t.Fatalf("local should not return token instead of %s", p.Token())
	}
	if p.TypeHost() != "local" {
		t.Fatalf("local should return good host instead of %s", p.TypeHost())
	}
	if p.Organisation() != "" {
		t.Fatalf("local should not return organisation instead of %s", p.Organisation())
	}
	if p.Name() != "testlocal" {
		t.Fatalf("local should return good name instead of %s", p.Name())
	}
	if p.GitURL() != path {
		t.Fatalf("local should return good url instead of %s", p.GitURL())
	}
	if p.AbsoluteDirectoy() != "/tmp/testlocal" {
		t.Fatalf("local should take path to write from path instead of %s", p.AbsoluteDirectoy())
	}
	if p.BranchName() != "branchName" {
		t.Fatalf("local should take branch name from config instead of %s", p.BranchName())
	}
	err = os.RemoveAll("/tmp/testlocal")
	if err != nil {
		t.Fatal("Can't remove /tmp/testlocal !?", err)
	}
}

func TestLocalDownload(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	l := newLocal(conf).(local)
	fakeGit := &mocks.FakeGit{
		DiffHook: func() bool {
			return false
		},
	}
	fakeProject := &mocks.FakeProject{
		GitHook: func() (ident1 project.Git) {
			return fakeGit
		},
	}
	p, err := l.download(fakeProject)
	if p == nil {
		t.Fatal("Good project should be return")
	}
	if err != nil {
		t.Fatal("Good project should not throw error")
	}
	fakeProject.AssertGitCalledOnce(t)
	fakeGit.AssertDiffCalledOnce(t)
}

func TestLocalDownloadWithGitModif(t *testing.T) {
	conf := Config{PoolSize: 1, PathToWrite: "/pathtowrite", BranchName: "branchName", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}
	l := newLocal(conf).(local)
	fakeGit := &mocks.FakeGit{
		DiffHook: func() bool {
			return true
		},
	}
	fakeProject := &mocks.FakeProject{
		GitHook: func() (ident1 project.Git) {
			return fakeGit
		},
	}
	p, err := l.download(fakeProject)
	if p == nil {
		t.Fatal("Good project should be return")
	}
	if err == nil {
		t.Fatal("Local project with git diff should throw error")
	}
	fakeProject.AssertGitCalledOnce(t)
	fakeGit.AssertDiffCalledOnce(t)
}
