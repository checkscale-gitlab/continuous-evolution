package deleter

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"errors"
	"os/exec"

	"github.com/sirupsen/logrus"
)

var logger = logrus.WithField("logger", "deleter/deleter")

//Delete the directory of a project. Use docker to avoid right conflict (because some file are created/modified by docker).
func Delete(o project.Option) project.Option {
	return o.Exec(func(p project.Project, _ error) (project.Project, error) {
		if p.AbsoluteDirectoy() == "/" {
			return p, errors.New("Can't delete path /")
		}
		volume := p.AbsoluteDirectoy()
		//Docker will remove all data in dir
		tools.NewDocker().StartDocker("alpine", []string{"rm", "-rf", "/data/"}, []string{volume + ":/data"}, "/data", []string{}, []string{})
		//Finally rmdir finish to clean the directory
		cmd := exec.Command("rm", "-rf", p.AbsoluteDirectoy())
		if err := cmd.Run(); err != nil {
			logger.WithField("path", p.AbsoluteDirectoy()).WithError(err).Error("Error making rmdir")
			return p, err
		}
		return p, nil
	})
}
