// generated by "charlatan -output=../mocks/projectAlreadySave.go -package=mocks ProjectAlreadySave".  DO NOT EDIT.

package mocks

import "reflect"

import "continuous-evolution/src/project"

// ProjectAlreadySaveIsEnabledInvocation represents a single call of FakeProjectAlreadySave.IsEnabled
type ProjectAlreadySaveIsEnabledInvocation struct {
	Results struct {
		Ident1 bool
	}
}

// ProjectAlreadySaveExistInvocation represents a single call of FakeProjectAlreadySave.Exist
type ProjectAlreadySaveExistInvocation struct {
	Parameters struct {
		Ident1 project.Project
	}
	Results struct {
		Ident2 bool
	}
}

// NewProjectAlreadySaveExistInvocation creates a new instance of ProjectAlreadySaveExistInvocation
func NewProjectAlreadySaveExistInvocation(ident1 project.Project, ident2 bool) *ProjectAlreadySaveExistInvocation {
	invocation := new(ProjectAlreadySaveExistInvocation)

	invocation.Parameters.Ident1 = ident1

	invocation.Results.Ident2 = ident2

	return invocation
}

// ProjectAlreadySaveTestingT represents the methods of "testing".T used by charlatan Fakes.  It avoids importing the testing package.
type ProjectAlreadySaveTestingT interface {
	Error(...interface{})
	Errorf(string, ...interface{})
	Fatal(...interface{})
	Helper()
}

/*
FakeProjectAlreadySave is a mock implementation of ProjectAlreadySave for testing.
Use it in your tests as in this example:

	package example

	func TestWithProjectAlreadySave(t *testing.T) {
		f := &mocks.FakeProjectAlreadySave{
			IsEnabledHook: func() (ident1 bool) {
				// ensure parameters meet expections, signal errors using t, etc
				return
			},
		}

		// test code goes here ...

		// assert state of FakeIsEnabled ...
		f.AssertIsEnabledCalledOnce(t)
	}

Create anonymous function implementations for only those interface methods that
should be called in the code under test.  This will force a panic if any
unexpected calls are made to FakeIsEnabled.
*/
type FakeProjectAlreadySave struct {
	IsEnabledHook func() bool
	ExistHook     func(project.Project) bool

	IsEnabledCalls []*ProjectAlreadySaveIsEnabledInvocation
	ExistCalls     []*ProjectAlreadySaveExistInvocation
}

// NewFakeProjectAlreadySaveDefaultPanic returns an instance of FakeProjectAlreadySave with all hooks configured to panic
func NewFakeProjectAlreadySaveDefaultPanic() *FakeProjectAlreadySave {
	return &FakeProjectAlreadySave{
		IsEnabledHook: func() (ident1 bool) {
			panic("Unexpected call to ProjectAlreadySave.IsEnabled")
		},
		ExistHook: func(project.Project) (ident2 bool) {
			panic("Unexpected call to ProjectAlreadySave.Exist")
		},
	}
}

// NewFakeProjectAlreadySaveDefaultFatal returns an instance of FakeProjectAlreadySave with all hooks configured to call t.Fatal
func NewFakeProjectAlreadySaveDefaultFatal(t_sym1 ProjectAlreadySaveTestingT) *FakeProjectAlreadySave {
	return &FakeProjectAlreadySave{
		IsEnabledHook: func() (ident1 bool) {
			t_sym1.Fatal("Unexpected call to ProjectAlreadySave.IsEnabled")
			return
		},
		ExistHook: func(project.Project) (ident2 bool) {
			t_sym1.Fatal("Unexpected call to ProjectAlreadySave.Exist")
			return
		},
	}
}

// NewFakeProjectAlreadySaveDefaultError returns an instance of FakeProjectAlreadySave with all hooks configured to call t.Error
func NewFakeProjectAlreadySaveDefaultError(t_sym2 ProjectAlreadySaveTestingT) *FakeProjectAlreadySave {
	return &FakeProjectAlreadySave{
		IsEnabledHook: func() (ident1 bool) {
			t_sym2.Error("Unexpected call to ProjectAlreadySave.IsEnabled")
			return
		},
		ExistHook: func(project.Project) (ident2 bool) {
			t_sym2.Error("Unexpected call to ProjectAlreadySave.Exist")
			return
		},
	}
}

func (f *FakeProjectAlreadySave) Reset() {
	f.IsEnabledCalls = []*ProjectAlreadySaveIsEnabledInvocation{}
	f.ExistCalls = []*ProjectAlreadySaveExistInvocation{}
}

func (f_sym3 *FakeProjectAlreadySave) IsEnabled() (ident1 bool) {
	if f_sym3.IsEnabledHook == nil {
		panic("ProjectAlreadySave.IsEnabled() called but FakeProjectAlreadySave.IsEnabledHook is nil")
	}

	invocation_sym3 := new(ProjectAlreadySaveIsEnabledInvocation)
	f_sym3.IsEnabledCalls = append(f_sym3.IsEnabledCalls, invocation_sym3)

	ident1 = f_sym3.IsEnabledHook()

	invocation_sym3.Results.Ident1 = ident1

	return
}

// SetIsEnabledStub configures ProjectAlreadySave.IsEnabled to always return the given values
func (f_sym4 *FakeProjectAlreadySave) SetIsEnabledStub(ident1 bool) {
	f_sym4.IsEnabledHook = func() bool {
		return ident1
	}
}

// IsEnabledCalled returns true if FakeProjectAlreadySave.IsEnabled was called
func (f *FakeProjectAlreadySave) IsEnabledCalled() bool {
	return len(f.IsEnabledCalls) != 0
}

// AssertIsEnabledCalled calls t.Error if FakeProjectAlreadySave.IsEnabled was not called
func (f *FakeProjectAlreadySave) AssertIsEnabledCalled(t ProjectAlreadySaveTestingT) {
	t.Helper()
	if len(f.IsEnabledCalls) == 0 {
		t.Error("FakeProjectAlreadySave.IsEnabled not called, expected at least one")
	}
}

// IsEnabledNotCalled returns true if FakeProjectAlreadySave.IsEnabled was not called
func (f *FakeProjectAlreadySave) IsEnabledNotCalled() bool {
	return len(f.IsEnabledCalls) == 0
}

// AssertIsEnabledNotCalled calls t.Error if FakeProjectAlreadySave.IsEnabled was called
func (f *FakeProjectAlreadySave) AssertIsEnabledNotCalled(t ProjectAlreadySaveTestingT) {
	t.Helper()
	if len(f.IsEnabledCalls) != 0 {
		t.Error("FakeProjectAlreadySave.IsEnabled called, expected none")
	}
}

// IsEnabledCalledOnce returns true if FakeProjectAlreadySave.IsEnabled was called exactly once
func (f *FakeProjectAlreadySave) IsEnabledCalledOnce() bool {
	return len(f.IsEnabledCalls) == 1
}

// AssertIsEnabledCalledOnce calls t.Error if FakeProjectAlreadySave.IsEnabled was not called exactly once
func (f *FakeProjectAlreadySave) AssertIsEnabledCalledOnce(t ProjectAlreadySaveTestingT) {
	t.Helper()
	if len(f.IsEnabledCalls) != 1 {
		t.Errorf("FakeProjectAlreadySave.IsEnabled called %d times, expected 1", len(f.IsEnabledCalls))
	}
}

// IsEnabledCalledN returns true if FakeProjectAlreadySave.IsEnabled was called at least n times
func (f *FakeProjectAlreadySave) IsEnabledCalledN(n int) bool {
	return len(f.IsEnabledCalls) >= n
}

// AssertIsEnabledCalledN calls t.Error if FakeProjectAlreadySave.IsEnabled was called less than n times
func (f *FakeProjectAlreadySave) AssertIsEnabledCalledN(t ProjectAlreadySaveTestingT, n int) {
	t.Helper()
	if len(f.IsEnabledCalls) < n {
		t.Errorf("FakeProjectAlreadySave.IsEnabled called %d times, expected >= %d", len(f.IsEnabledCalls), n)
	}
}

func (f_sym5 *FakeProjectAlreadySave) Exist(ident1 project.Project) (ident2 bool) {
	if f_sym5.ExistHook == nil {
		panic("ProjectAlreadySave.Exist() called but FakeProjectAlreadySave.ExistHook is nil")
	}

	invocation_sym5 := new(ProjectAlreadySaveExistInvocation)
	f_sym5.ExistCalls = append(f_sym5.ExistCalls, invocation_sym5)

	invocation_sym5.Parameters.Ident1 = ident1

	ident2 = f_sym5.ExistHook(ident1)

	invocation_sym5.Results.Ident2 = ident2

	return
}

// SetExistStub configures ProjectAlreadySave.Exist to always return the given values
func (f_sym6 *FakeProjectAlreadySave) SetExistStub(ident2 bool) {
	f_sym6.ExistHook = func(project.Project) bool {
		return ident2
	}
}

// SetExistInvocation configures ProjectAlreadySave.Exist to return the given results when called with the given parameters
// If no match is found for an invocation the result(s) of the fallback function are returned
func (f_sym7 *FakeProjectAlreadySave) SetExistInvocation(calls_sym7 []*ProjectAlreadySaveExistInvocation, fallback_sym7 func() bool) {
	f_sym7.ExistHook = func(ident1 project.Project) (ident2 bool) {
		for _, call_sym7 := range calls_sym7 {
			if reflect.DeepEqual(call_sym7.Parameters.Ident1, ident1) {
				ident2 = call_sym7.Results.Ident2

				return
			}
		}

		return fallback_sym7()
	}
}

// ExistCalled returns true if FakeProjectAlreadySave.Exist was called
func (f *FakeProjectAlreadySave) ExistCalled() bool {
	return len(f.ExistCalls) != 0
}

// AssertExistCalled calls t.Error if FakeProjectAlreadySave.Exist was not called
func (f *FakeProjectAlreadySave) AssertExistCalled(t ProjectAlreadySaveTestingT) {
	t.Helper()
	if len(f.ExistCalls) == 0 {
		t.Error("FakeProjectAlreadySave.Exist not called, expected at least one")
	}
}

// ExistNotCalled returns true if FakeProjectAlreadySave.Exist was not called
func (f *FakeProjectAlreadySave) ExistNotCalled() bool {
	return len(f.ExistCalls) == 0
}

// AssertExistNotCalled calls t.Error if FakeProjectAlreadySave.Exist was called
func (f *FakeProjectAlreadySave) AssertExistNotCalled(t ProjectAlreadySaveTestingT) {
	t.Helper()
	if len(f.ExistCalls) != 0 {
		t.Error("FakeProjectAlreadySave.Exist called, expected none")
	}
}

// ExistCalledOnce returns true if FakeProjectAlreadySave.Exist was called exactly once
func (f *FakeProjectAlreadySave) ExistCalledOnce() bool {
	return len(f.ExistCalls) == 1
}

// AssertExistCalledOnce calls t.Error if FakeProjectAlreadySave.Exist was not called exactly once
func (f *FakeProjectAlreadySave) AssertExistCalledOnce(t ProjectAlreadySaveTestingT) {
	t.Helper()
	if len(f.ExistCalls) != 1 {
		t.Errorf("FakeProjectAlreadySave.Exist called %d times, expected 1", len(f.ExistCalls))
	}
}

// ExistCalledN returns true if FakeProjectAlreadySave.Exist was called at least n times
func (f *FakeProjectAlreadySave) ExistCalledN(n int) bool {
	return len(f.ExistCalls) >= n
}

// AssertExistCalledN calls t.Error if FakeProjectAlreadySave.Exist was called less than n times
func (f *FakeProjectAlreadySave) AssertExistCalledN(t ProjectAlreadySaveTestingT, n int) {
	t.Helper()
	if len(f.ExistCalls) < n {
		t.Errorf("FakeProjectAlreadySave.Exist called %d times, expected >= %d", len(f.ExistCalls), n)
	}
}

// ExistCalledWith returns true if FakeProjectAlreadySave.Exist was called with the given values
func (f_sym8 *FakeProjectAlreadySave) ExistCalledWith(ident1 project.Project) bool {
	for _, call_sym8 := range f_sym8.ExistCalls {
		if reflect.DeepEqual(call_sym8.Parameters.Ident1, ident1) {
			return true
		}
	}

	return false
}

// AssertExistCalledWith calls t.Error if FakeProjectAlreadySave.Exist was not called with the given values
func (f_sym9 *FakeProjectAlreadySave) AssertExistCalledWith(t ProjectAlreadySaveTestingT, ident1 project.Project) {
	t.Helper()
	var found_sym9 bool
	for _, call_sym9 := range f_sym9.ExistCalls {
		if reflect.DeepEqual(call_sym9.Parameters.Ident1, ident1) {
			found_sym9 = true
			break
		}
	}

	if !found_sym9 {
		t.Error("FakeProjectAlreadySave.Exist not called with expected parameters")
	}
}

// ExistCalledOnceWith returns true if FakeProjectAlreadySave.Exist was called exactly once with the given values
func (f_sym10 *FakeProjectAlreadySave) ExistCalledOnceWith(ident1 project.Project) bool {
	var count_sym10 int
	for _, call_sym10 := range f_sym10.ExistCalls {
		if reflect.DeepEqual(call_sym10.Parameters.Ident1, ident1) {
			count_sym10++
		}
	}

	return count_sym10 == 1
}

// AssertExistCalledOnceWith calls t.Error if FakeProjectAlreadySave.Exist was not called exactly once with the given values
func (f_sym11 *FakeProjectAlreadySave) AssertExistCalledOnceWith(t ProjectAlreadySaveTestingT, ident1 project.Project) {
	t.Helper()
	var count_sym11 int
	for _, call_sym11 := range f_sym11.ExistCalls {
		if reflect.DeepEqual(call_sym11.Parameters.Ident1, ident1) {
			count_sym11++
		}
	}

	if count_sym11 != 1 {
		t.Errorf("FakeProjectAlreadySave.Exist called %d times with expected parameters, expected one", count_sym11)
	}
}

// ExistResultsForCall returns the result values for the first call to FakeProjectAlreadySave.Exist with the given values
func (f_sym12 *FakeProjectAlreadySave) ExistResultsForCall(ident1 project.Project) (ident2 bool, found_sym12 bool) {
	for _, call_sym12 := range f_sym12.ExistCalls {
		if reflect.DeepEqual(call_sym12.Parameters.Ident1, ident1) {
			ident2 = call_sym12.Results.Ident2
			found_sym12 = true
			break
		}
	}

	return
}
