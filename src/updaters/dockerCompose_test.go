package updaters

import (
	"continuous-evolution/src/project"
	"errors"
	"testing"
)

func TestDockerComposeIsPackageFile(t *testing.T) {
	res := dockerComposeIsPackageFile("docker-compose.yml", fakeFileInfo{name: "docker-compose.yml", isDir: false})
	if !res {
		t.Fatal("docker-compose updater should return true for docker-compose.yml")
	}
	res = dockerComposeIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("docker-compose updater should return false for other")
	}
	res = dockerComposeIsPackageFile("docker-compose.yml", fakeFileInfo{name: "docker-compose.yml", isDir: true})
	if res {
		t.Fatal("docker-compose updater should return false for directory even if its name is docker-compose.yml")
	}
}

const data = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  conevol:
    image:conevol
`

const goodData = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq:1.0
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  conevol:
    image: conevol:0.2

`

func TestUpdateDockerCompose(t *testing.T) {
	d := newDockerCompose(DefaultConfig)
	d.(*dockerComposeUpdater).fetchLatestDockerTag = func(pkg dockerPkg) (string, error) {
		if pkg.scope == "nsqio" && pkg.project == "nsq" {
			return "1.0", nil
		} else if pkg.scope == "library" && pkg.project == "conevol" {
			return "0.2", nil
		}
		return "", errors.New("You call method with bad argument")
	}
	_, newData := d.(*dockerComposeUpdater).updateServices([]byte(data), []string{})
	if newData != goodData {
		t.Fatal("Bad updated data", newData)
	}
}

const dataMultipleSameService = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  nsqd:
    image: nsqio/nsq
    command: /nsqd --lookupd-tcp-address=nsqlookupd:4160
    ports:
      - "4150:4150"
      - "4151"
    depends_on: 
      - nsqlookupd

  conevol:
    image: conevol:0.2
`

const goodDataMultipleSameService = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq:1.0
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  nsqd:
    image: nsqio/nsq:1.0
    command: /nsqd --lookupd-tcp-address=nsqlookupd:4160
    ports:
      - "4150:4150"
      - "4151"
    depends_on: 
      - nsqlookupd

  conevol:
    image: conevol:0.8

`

type dataCheckTest struct {
	Name            string
	OriginalVersion string
	NewVersion      string
}

func check(t *testing.T, toTest project.Dependency, truth dataCheckTest) {
	if toTest.Name != truth.Name {
		t.Fatalf("Should be %s instead of %s", truth.Name, toTest.Name)
	}
	if toTest.OriginalVersion != truth.OriginalVersion {
		t.Fatalf("Should be %s instead of %s", truth.OriginalVersion, toTest.OriginalVersion)
	}
	if toTest.NewVersion != truth.NewVersion {
		t.Fatalf("Should be %s instead of %s", truth.NewVersion, toTest.NewVersion)
	}
}

func TestUpdateDockerComposeMultiple(t *testing.T) {
	d := newDockerCompose(DefaultConfig)
	d.(*dockerComposeUpdater).fetchLatestDockerTag = func(pkg dockerPkg) (string, error) {
		if pkg.scope == "nsqio" && pkg.project == "nsq" {
			return "1.0", nil
		} else if pkg.scope == "library" && pkg.project == "conevol" && pkg.oldVersion == "0.2" {
			return "0.8", nil
		}
		return "", errors.New("You call method with bad argument")
	}
	dependencies, newData := d.(*dockerComposeUpdater).updateServices([]byte(dataMultipleSameService), []string{})
	if newData != goodDataMultipleSameService {
		t.Fatal("Bad updated data", newData)
	}
	if len(dependencies) != 3 {
		t.Fatal("Should return 3 dep")
	}
	check(t, dependencies[0], dataCheckTest{Name: "nsqio/nsq", OriginalVersion: "latest", NewVersion: "1.0"})
	check(t, dependencies[1], dataCheckTest{Name: "nsqio/nsq", OriginalVersion: "latest", NewVersion: "1.0"})
	check(t, dependencies[2], dataCheckTest{Name: "conevol", OriginalVersion: "0.2", NewVersion: "0.8"})
}

const goodDataMultipleServiceExcudes = `version: '3'

services:
  nsqlookupd:
    image: nsqio/nsq:1.0
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"

  nsqd:
    image: nsqio/nsq:1.0
    command: /nsqd --lookupd-tcp-address=nsqlookupd:4160
    ports:
      - "4150:4150"
      - "4151"
    depends_on: 
      - nsqlookupd

  conevol:
    image: conevol:0.2

`

func TestUpdateDockerComposeMultipleWithExcludes(t *testing.T) {
	d := newDockerCompose(DefaultConfig)
	d.(*dockerComposeUpdater).fetchLatestDockerTag = func(pkg dockerPkg) (string, error) {
		if pkg.scope == "nsqio" && pkg.project == "nsq" {
			return "1.0", nil
		} else if pkg.scope == "library" && pkg.project == "conevol" && pkg.oldVersion == "0.2" {
			return "0.8", nil
		}
		return "", errors.New("You call method with bad argument")
	}
	dependencies, newData := d.(*dockerComposeUpdater).updateServices([]byte(dataMultipleSameService), []string{"conevol"})
	if newData != goodDataMultipleServiceExcudes {
		t.Fatal("Bad updated data", newData)
	}
	if len(dependencies) != 2 {
		t.Fatal("Should return 2 dep")
	}
	check(t, dependencies[0], dataCheckTest{Name: "nsqio/nsq", OriginalVersion: "latest", NewVersion: "1.0"})
	check(t, dependencies[0], dataCheckTest{Name: "nsqio/nsq", OriginalVersion: "latest", NewVersion: "1.0"})
}
