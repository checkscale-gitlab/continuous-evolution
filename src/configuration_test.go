package main

import (
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/orchestrator"
	"continuous-evolution/src/reporters"
	"continuous-evolution/src/scheduler"
	"continuous-evolution/src/updaters"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

func defaultConf() map[string]interface{} {
	toCompare := make(map[string]interface{})
	toCompare["Version"] = "1.0.0"
	toCompare["Orchestrator"] = orchestrator.DefaultConfig
	toCompare["Downloader"] = downloader.DefaultConfig
	toCompare["Updater"] = updaters.DefaultConfig
	toCompare["Reporter"] = reporters.DefaultConfig
	toCompare["Scheduler"] = scheduler.DefaultConfig
	return toCompare
}

func helperCompare(c interface{}, toCompare map[string]interface{}, t *testing.T) {
	meta := reflect.ValueOf(c)
	metaType := reflect.TypeOf(c)
	for i := 0; i < metaType.NumField(); i++ {
		name := metaType.Field(i).Name
		returned := meta.FieldByName(name).Interface()
		if !reflect.DeepEqual(returned, toCompare[name]) {
			t.Fatalf("Default config should contain conf : %#v instead of %#v for %s", returned, toCompare[name], name)
		}
	}
}

func TestDefaultConfig(t *testing.T) {
	c, err := GetConfig("")
	if err != nil {
		t.Fatal("No path should not return error but default conf", err)
	}
	helperCompare(c, defaultConf(), t)
}

func TestWriteConfig(t *testing.T) {
	err := WriteConf("/tmp/writeDefaultConfig.toml")
	defer func() {
		os.Remove("/tmp/writeDefaultConfig.toml")
	}()
	if err != nil {
		t.Fatal("Can't write default conf", err)
	}
	c, err := GetConfig("/tmp/writeDefaultConfig.toml")
	if err != nil {
		t.Fatal("Path generated should not return error but default conf", err)
	}
	helperCompare(c, defaultConf(), t)
}

func TestConfigFromFileEmpty(t *testing.T) {
	err := ioutil.WriteFile("/tmp/configTestFromFile.toml", []byte(`version="1.0.0"`), 0777)
	defer func() {
		os.Remove("/tmp/configTestFromFile.toml")
	}()
	if err != nil {
		t.Fatal("Can't write conf file", err)
	}
	c, err := GetConfig("/tmp/configTestFromFile.toml")
	if err != nil {
		t.Fatal("No path should not return error but default conf", err)
	}
	helperCompare(c, defaultConf(), t)
}

func TestConfigFromFileOnlySomeOverride(t *testing.T) {
	data := `version="1.0.0"
	[scheduler]
	enabled=true
	pathdb="/path/to/db"
	waitdurationprocess="1m"# 1 week
	`
	err := ioutil.WriteFile("/tmp/configTestFromFileOnlySomeOverride.toml", []byte(data), 0777)
	defer func() {
		os.Remove("/tmp/configTestFromFileOnlySomeOverride.toml")
	}()
	if err != nil {
		t.Fatal("Can't write conf file", err)
	}
	c, err := GetConfig("/tmp/configTestFromFileOnlySomeOverride.toml")
	if err != nil {
		t.Fatal("No path should not return error but default conf", err)
	}

	toCompare := defaultConf()
	toCompare["Scheduler"] = scheduler.Config{Enabled: true, PathDb: "/path/to/db", WaitDurationProcess: "1m"}

	helperCompare(c, toCompare, t)
}

func TestConfigNotExistingFile(t *testing.T) {
	_, err := GetConfig("/tmp/notExistingConfig.toml")
	if err == nil {
		t.Fatal("Not existing file should return err")
	}
}

func TestConfigNotTomlFile(t *testing.T) {
	data := `[scheduler]
	enabled=123
	pathdb=123
	waitdurationprocess=123`
	err := ioutil.WriteFile("/tmp/confifBadToml.toml", []byte(data), 0777)
	defer func() {
		os.Remove("/tmp/confifBadToml.toml")
	}()
	if err != nil {
		t.Fatal("Can't write conf file", err)
	}
	_, err = GetConfig("/tmp/confifBadToml.toml")
	if err == nil {
		t.Fatal("Bad toml conf file should return err")
	}
}

func TestConfigNoVersion(t *testing.T) {
	data := `[scheduler]
	enabled=true
	pathdb="/path/to/db"
	waitdurationprocess="1m"# 1 week`
	err := ioutil.WriteFile("/tmp/configNoVersion.toml", []byte(data), 0777)
	defer func() {
		os.Remove("/tmp/configNoVersion.toml")
	}()
	if err != nil {
		t.Fatal("Can't write conf file", err)
	}
	_, err = GetConfig("/tmp/configNoVersion.toml")
	if err == nil {
		t.Fatal("Config without version should return err")
	}
}

func TestConfigBadVersion(t *testing.T) {
	data := `version="0.0.1"
	[scheduler]
	enabled=true
	pathdb="/path/to/db"
	waitdurationprocess="1m"# 1 week`
	err := ioutil.WriteFile("/tmp/configBadVersion.toml", []byte(data), 0777)
	defer func() {
		os.Remove("/tmp/configBadVersion.toml")
	}()
	if err != nil {
		t.Fatal("Can't write conf file", err)
	}
	_, err = GetConfig("/tmp/configBadVersion.toml")
	if err == nil {
		t.Fatal("Config with bad version should return err")
	}
}
